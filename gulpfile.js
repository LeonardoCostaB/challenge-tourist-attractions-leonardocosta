const {
  src,
  dest,
  watch,
  parallel
} = require('gulp')
const sass = require('gulp-sass')(require('sass'))
const imagemin = require('gulp-imagemin')
const browserify = require('browserify')
const babelify = require('babelify')
const source = require('vinyl-source-stream')
const uglify = require('gulp-uglify')
const buffer = require('vinyl-buffer')
const connect = require('gulp-connect')

const paths = {
  scripts: {
    src: "src/js/index.js",
    watch: "src/js/**/*.js",
  },

  styles: {
    src: "src/scss/main.scss",
    watch: "src/scss/**/*.scss"
  },

  img: {
    src: "src/assets/**/*",
  },

  html: {
    src: "src/pages/index.html",
  },
  
  dest: "dist",
  temp: ".tmp",
};

function server() {
  connect.server({
    root: 'dist',
    livereload: true,
    port: 3000
  })
}

function updatingFileChanges() {
  watch(
    paths.html.src, 
    { ignoreInitial: false}, 
    htmlFiles
  )

  watch(
    paths.img.src, 
    { ignoreInitial: false }, 
    imgSquash
  );

  watch(
    paths.styles.watch, 
    { ignoreInitial: false},
    sassFiles
  )
  
  watch(
    paths.scripts.watch, 
    { ignoreInitial: false},
    scriptsFiles
  )
}

function htmlFiles() {
  return src(paths.html.src)
  .pipe(dest('dist'))
  .pipe(connect.reload())
}

function imgSquash() {
  return src(paths.img.src)
  .pipe(imagemin())
  .pipe(dest('dist/assets'))
}

function sassFiles() {
  return src(paths.styles.src)
  .pipe(
    sass({ 
      outputStyle: 'compressed' 
    }).on('error', sass.logError)
  )
  .pipe(dest('dist'))
  .pipe(connect.reload())
}

function scriptsFiles() {
  return browserify(paths.scripts.src)
  .transform(
    babelify.configure({
      presets: ['@babel/preset-env']
    })
  )
  .bundle()
  .pipe(source('bundle.js'))
  .pipe(buffer())
  .pipe(uglify())
  .pipe(dest('dist'))
  .pipe(connect.reload())
}

exports.default = parallel(server, updatingFileChanges)