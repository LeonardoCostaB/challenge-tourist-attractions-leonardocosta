# Descrição do projeto

--- Arquitetura

html: {
  - Todo o html do projeto está localizada na pasta pages
}

css: {
  - Dentro da pasta scss está localizado css.
  - Pasta global: {
    - Dentro da pasta global está a refêrencia de todo css global, tais como, funções, variáveis e também os resetes no css padrão do navegador.
  }
  - Pasta partials: {
    - Essa pasta contém o css da aplicação divida em partes para ficar melhor legível.
  }
}

js: {
  - Dentro da pasta js está localizado o javaScript.
  - Pasta tourism: {
    - O arquivo touristSpot tem uma classe que é responsável somente em adicionar os pontos turísticos em minha aplicação.
    - O arquivo createNewTourismSpot tem uma classe que é responsável somente em capturar os dados quem vem do front end e envia-lo para a classe que é responsável em adicionar no html.
  }
  - O arquivo index fica responsável por fazer toda o javascript da aplicação inicializar.
}

---- Fluxo

Antes de começar a codar, eu pensei em implementar um fluxo para que cada classe tenha sua responsabilidade, assim, escrever o código de uma forma que não seria ruim de dar manutenção, ser fácil de compreender, para também treinar minha lógica e tudo que eu venho estudando. 

Então o fluxo que achei mais viável foi dessa forma, tendo uma classe que seria responsável somente por implementar os pontos turísticos no html e uma outra classe que capturasse os dados, tratasse e enviasse para a classe que fica responsável por aplicar esses dados no html.

---- Responsabilidade de cada classe

A classe principal ela recebe os dados, salvar no localstorage, captura esses dados salvos e enviar para o html.

A classe que recebe os dados vindo do formulário, ela vai tratar para que tudo fique legível e vai enviar para a classe principal.