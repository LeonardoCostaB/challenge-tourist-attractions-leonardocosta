// Essa classe tem como sua finalidade implementar os pontos turísticos no html

export class TouristSpot {
  constructor() {
    this.setTouristSpot();
    this.getLocalStorage()
  }
  // função responsável por introduzir os pontos turísticos que já vem no projeto
  setTouristSpot() {
    const spot = [
      {
        image: {
          src: '../../assets/pao_de_acucar.svg',
          srcset: '../../assets/mobile/pao_de_acucar_mobile.svg',
          alt: 'imagem do pão de açucar, um dos pontos mais requisitados do Rio de Janeiro.',
        },

        title: 'Pão de Açucar',

        description: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.'
      },

      {
        image: {
          src: '../../assets/ilha_grande.svg',
          srcset: '../../assets/mobile/ilha_grande_mobile.svg',
          alt: 'Ilha Grande é uma ilha no estado brasileiro do Rio de Janeiro rodeada de praias, coberta pela Mata Atlântica e atravessada por trilhos sinuosos.'
        },

        title: 'Ilha Grande',

        description: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.'
      },

      {
        image: {
          src: '../../assets/cristo_redentor.svg',
          srcset: '../../assets/mobile/cristo_redentor_mobile.svg',
          alt: 'Cristo Redentor é uma estátua art déco que retrata Jesus Cristo, localizada no topo do morro do Corcovado, a 709 metros acima do nível do mar, com vista para parte considerável da cidade brasileira do Rio de Janeiro.'
        },

        title: 'Cristo Redentor',

        description: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.'
      },

      {
        image: {
          src: '../../assets/parati.svg',
          srcset: '../../assets/mobile/parati_mobile.svg',
          alt: 'Cidade antiga com ruas de paralelepípedo, casas históricas, igrejas, lojas de artesanato e restaurantes.'
        },

        title: 'Centro Histórico de Paraty',

        description: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.'
      }
    ]

    const root = document.querySelector('#root')

    spot.map(spot => {
      const createTourismSport = document.createElement('div')
      createTourismSport.setAttribute('class', 'tourism-spot')
      
      const html = `
        <div class="image">
          <picture>
            <source 
              media="(min-width: 1024px)" 
              srcset="${spot.image.srcset}"
            />

            <img 
              src="${spot.image.srcset}" 
              alt="${spot.image.alt}"
            />
          </picture>
        </div>

        <div class="description">
          <h3>${spot.title}</h3>

          <p>${spot.description}</p>
        </div>
      `
      
      createTourismSport.innerHTML = html
      
      root.appendChild(createTourismSport)
    })
  }

  // função responsável por capturar os dados do localStorage e implementar no html
  getLocalStorage() {
    const root = document.querySelector('#root')

    for (let i = 0; i < localStorage.length; i++){
      let key = localStorage.key(i);
      let value = JSON.parse(localStorage.getItem(key))
            
      const createTourismSport = document.createElement('div')
      createTourismSport.setAttribute('class', 'tourism-spot')
      
      const html = `
        <div class="image">
          <img 
            src="${value.src}" 
            alt="imagem do ponto turístico: ${value.title}"
            class="client-image"
          />
        </div>
  
        <div class="description">
          <h3>${value.title}</h3>
  
          <p>${value.description}</p>
        </div>
      `
      
      createTourismSport.innerHTML = html
      
      root.appendChild(createTourismSport)
    }
  }

  // função responsável por salvar no localStorage
  savingLocalStorage({
    src,
    title,
    description
  }) {
    const information = {
      src,
      title,
      description
    }

    localStorage.setItem(information.title, JSON.stringify(information))
  }
}