import { TouristSpot } from './touristSpot.js'

// Essa classe tem como sua finalidade capturar os dados vindo do front end e os dados para a classe principal

export class CreateNewTourismSpot extends TouristSpot {
  constructor() {
    super()
    
    this.postDataTouristSpot();
    this.getUrlImage();
  }

  // função responsável por capturar a url da imagem, torna-la legível e enviar por parãmetro para a função postDataTourismSpot
  getUrlImage() {
    const container = document.querySelector('.input-image')
    const photoTouristSpot = document.querySelector('#imageTourist')
    const inputTouristSpot =  document.querySelector('#touristSpotImage')

    container.addEventListener('click', () => {
      inputTouristSpot.click() 
    })

    /* 
      tornado a div container onde clico para adicionar a imagem 
      acessível para quem depende da navegação por teclado 
    */
    container.addEventListener('keypress', (e) => {
      e.key == 'Enter' ? inputTouristSpot.click()  : ''
    })

    inputTouristSpot.addEventListener('change', (e) => {
      const reader = new FileReader()

      reader.onload = () => {
        photoTouristSpot.src = reader.result
        photoTouristSpot.classList.add('visibily')

        container.querySelector('span').style.display = 'none'
        
        this.postDataTouristSpot(reader.result)
      }

      reader.readAsDataURL(e.target.files[0])
    })
  }

  // Essa função fica responsável por receber os dados do formulário e também da função getUrlImage e mandar para classe principal pela a função que salva no localStorage
  postDataTouristSpot(urlImage) {
    const handleSubmitForm = (e) => {
      const formData = new FormData(e.target);

      let src = urlImage
      let title =  formData.getAll('touristSpotName')
      let description = formData.getAll('touristSpotDescription')

      this.savingLocalStorage({
        src, 
        title, 
        description
      })
    }

    const form = document.querySelector("#touristSpot");
    form.addEventListener('submit', handleSubmitForm);
  }
}